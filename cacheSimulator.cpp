/*
 * cacheSimulator.cpp
 *
 *  Created on: 26-Sep-2015
 *      Author: Lenovo
 */

#include "cacheSimulator.h"

CcacheSimulator::CcacheSimulator(CcacheSimulator* nextLevelCache,int blockSize,int cacheSize,int associativity,int victimCacheSize) {
	// TODO Auto-generated constructor stub
	m_blockSize = blockSize;
	m_cacheSize = cacheSize;
	m_cacheAssoc = associativity;
	m_cacheRows = m_cacheSize/(m_cacheAssoc*m_blockSize);
	m_cache = new memBlock*[m_cacheRows];
	m_L2Cache = nextLevelCache;

	for (int i =0;i<m_cacheRows;i++){
		m_cache[i]=new memBlock[m_cacheAssoc]; //create cols
	}
	m_blockOffset = log2(m_blockSize);
	m_indexBits = log2(m_cacheRows);


	m_VC_cols = victimCacheSize;
	if(m_VC_cols >0)
	{
		//create VC
		m_VC_cache = new memBlock[m_VC_cols];
		m_victimCacheisEnabled = true;
	}
	else
		m_victimCacheisEnabled = false;

	//todo: remove it
	//	char addressBinary[32] = {'0','0','0','0',	'0','0','0','0', '0','0','1','0', '0','1','0','1',
	//			'0','1','0','0',  '1','0','0','0', '1','0','1','1', '0','1','1','0',};
	//m_cache[15][1].validBit = 1;
	//copy(addressBinary,addressBinary+32,m_cache[15][0].tag);

	m_readMissCounter  = 0;
	m_writeMissCounter = 0;
	m_readCounter = 0;
	m_writeCounter = 0;
	m_writeBackstoNextCache =0;
	m_numberofSwapReqs =0;
	m_numberofSwaps =0;
//
//	readFromCache("12a45B78");//miss
//	cout<<"missc counter="<<m_readMissCounter<<endl;
//	readFromCache("12a45a78");//miss
//	cout<<"missc counter="<<m_readMissCounter<<endl;
//	//writeToCache("12a45B78");//hit b* a
//	//cout<<"missc counter="<<m_writeMissCounter<<endl;
//	readFromCache("12a45c78");//miss b c* do sawp req
//	cout<<"missc counter="<<m_readMissCounter<<endl;
//	readFromCache("12a45a78");//miss a* c
//	cout<<"missc counter="<<m_readMissCounter<<endl;
//
//	readFromCache("12a45b78");//miss//swap shd happen
//	cout<<"missc counter="<<m_readMissCounter<<endl;
	/*writeToCache("12a45D78");//hit b* a
	cout<<"missc counter="<<m_writeMissCounter<<endl;
	writeToCache("12a45c78");//miss b c*
	cout<<"missc counter="<<m_writeMissCounter<<endl;

	writeToCache("12a45a78");//miss a* c
	cout<<"missc counter="<<m_writeMissCounter<<endl;
	readFromCache("12a45B78");//miss a b*
	cout<<"missc counter="<<m_writeMissCounter<<endl;
	writeToCache("12a45a78");//hit a* b
	cout<<"missc counter="<<m_writeMissCounter<<endl;
	readFromCache("12a45B78"); //hit a b*

	cout<<"missc counter="<<m_writeMissCounter<<endl;
	writeToCache("12a45B98"); //miss b*
	cout<<"missc counter="<<m_writeMissCounter<<endl;
	writeToCache("12a45c98"); //miss b *c
	cout<<"missc counter="<<m_writeMissCounter<<endl;
	writeToCache("12a45d98"); //miss d* c
	cout<<"missc counter="<<m_writeMissCounter<<endl;
	writeToCache("12a45d98"); //hit d* c
	cout<<"missc counter="<<m_writeMissCounter<<endl;
	writeToCache("12a45d98"); //hit d* c
	cout<<"missc counter="<<m_writeMissCounter<<endl;
	readFromCache("12a45c98"); //hit d c*
	cout<<"missc counter="<<m_writeMissCounter<<endl;*/
	//char a[5]= {'1','1','1','0','1'};

	//cout<<"HEX="<<getHexFromBinary(a)<<endl;
	//writeToCache("12a45a78");//miss
	//printCacheContents();
	//addressDecoder("12a45B78");
	//	addressDecoder("123456fe");
	//	addressDecoder("abcdef78");
	//	addressDecoder("a3cd2f21");
	//	int** array = new int*[rows];
	//
	//		for (int i =0;i<rows;i++){
	//			array[i]=new int[m_associativity];
	//			for(int j=0;j<m_associativity;j++)
	//				array[i][j] = i+j;
	//		}
	//		for (int i =0;i<rows;i++){
	//					for(int j=0;j<m_associativity;j++)
	//						std::cout<<"array i j="<<i<<j<<array[i][j]<<std::endl;
	//				}


}

CcacheSimulator::~CcacheSimulator() {
	// TODO Auto-generated destructor stub
}

void CcacheSimulator::writeToCache(string adress){
#ifdef __DEBUG_OUT
	cout<<"---------------------------------------"<<endl;
#endif
	m_writeCounter++;
	this->addressDecoder(adress); //current tag and set no are set
#ifdef __DEBUG_OUT
	cout<<"L1 write: "<<"(tag , index "<<m_currentSetNumber<<endl;
#endif
	int blockColNumber = findTheRequestedBlock();
#ifdef __DEBUG
	cout<<"blockColNumber="<<blockColNumber<<endl;
#endif
	if(blockColNumber == -1)//if it is a miss
	{
#ifdef __DEBUG_OUT
		cout<<"L1 miss "<<endl;
#endif

		//first increment the write miss counter
		m_writeMissCounter++;
		blockColNumber = findInvalidBlock(); //in the current set
#ifdef __DEBUG
		cout<<"invalidBlockNumber="<<blockColNumber<<endl;
#endif
		if(blockColNumber == -1) //thre is no invalid blk //corresponds to step1
		{
			//find LRU block the one with max LRU counter
			blockColNumber = findLRUBlockNumber();
			if(blockColNumber == -1) //not found
			{
#ifdef __DEBUG
				cout<<"ERRROR::LRU BLOCK NOT FOUND"<<endl;
#endif
				exit(0);
			}

			//todo::write to next level if it is dirty block
			//todo::write to next level if it is dirty block
			if(m_victimCacheisEnabled){
				if(swapFromVC(blockColNumber) == true)
				{
					//do nothing
					//m_writeMissCounter--; //we incremented it above..so decrement it
				}
				else{
					//read from L2
					if(m_L2Cache!= NULL){
						char appndedAddress[32];
						//cout<<"appendIndexTotheTag l2"<<endl;
						copy(m_currentAdressTag,m_currentAdressTag+32,appndedAddress); //copy the tag
						appendIndexandOffsetTotheTag(appndedAddress); //append the index
						//cout<<"appendIndexTotheTag l2 done"<<endl;
						m_L2Cache->readFromCache(getHexFromBinary(appndedAddress));
					}

				}
			}
			else
			{
				if(m_cache[m_currentSetNumber][blockColNumber].dirtyBit)//if it is dirty
				{
					//writeLRU_L1_toL2();
					if(m_L2Cache!= NULL){

						char appndedAddress[32];
						cout<<"appendIndexTotheTag l2"<<endl;
						copy(m_cache[m_currentSetNumber][blockColNumber].tag,m_cache[m_currentSetNumber][blockColNumber].tag+32,appndedAddress); //copy the tag
						appendIndexandOffsetTotheTag(appndedAddress); //append the index
						cout<<"appendIndexTotheTag l2 done"<<endl;
						m_L2Cache->writeToCache(getHexFromBinary(appndedAddress));
					}
					m_writeBackstoNextCache++;
				}
				//corresponds to step2..read from L2
				if(m_L2Cache!= NULL){

					char appndedAddress[32];
					cout<<"appendIndexTotheTag l2"<<endl;
					copy(m_currentAdressTag,m_currentAdressTag+32,appndedAddress); //copy the tag
					appendIndexandOffsetTotheTag(appndedAddress); //append the index
					cout<<"appendIndexTotheTag l2 done"<<endl;
					m_L2Cache->readFromCache(getHexFromBinary(appndedAddress));
				}
			}

		}
		else{
			//corresponds to step2
			if(m_L2Cache!= NULL){

				char appndedAddress[32];
				cout<<"appendIndexTotheTag l2"<<endl;
				copy(m_currentAdressTag,m_currentAdressTag+32,appndedAddress); //copy the tag
				appendIndexandOffsetTotheTag(appndedAddress); //append the index
				cout<<"appendIndexTotheTag l2 done"<<endl;
				m_L2Cache->readFromCache(getHexFromBinary(appndedAddress));
			}
		}
#ifdef __DEBUG
		cout<<"blockColNumber="<<blockColNumber<<endl;
#endif


	}
#ifdef __DEBUG_OUT
	cout<<"L1 victim:: None "<<endl;
	cout<<"L1 update LRU"<<endl;
#endif
	//update state
	m_cache[m_currentSetNumber][blockColNumber].validBit = 1; //valid
#ifdef __DEBUG_OUT
	cout<<"L1 set dirty"<<endl;
#endif
	m_cache[m_currentSetNumber][blockColNumber].dirtyBit = 1; //it is dirty
	copy(m_currentAdressTag,m_currentAdressTag+32,m_cache[m_currentSetNumber][blockColNumber].tag); //tag


	//update LRU counter others
	updateLRUCounter(m_cache[m_currentSetNumber][blockColNumber].lruCounter);
	//update LRU counter
	m_cache[m_currentSetNumber][blockColNumber].lruCounter = 0; //most recenlty used

#ifdef __DEBUG
	for(int i =0;i<m_cacheAssoc;i++) //traverse through the cols
	{
		cout<<"lru count_"<<i<<"= "<<m_cache[m_currentSetNumber][i].lruCounter<<"";
		cout<<"DB_"<<i<<"= "<<m_cache[m_currentSetNumber][i].dirtyBit<<"	";
	}

	cout<<"write to Cache block found ="<<blockColNumber;
#endif
}









void CcacheSimulator::readFromCache(string adress){

#ifdef __DEBUG_OUT
	cout<<"---------------------------------------"<<endl;
#endif

	m_readCounter++;
	this->addressDecoder(adress); //current tag and set no are set

#ifdef __DEBUG_OUT
	cout<<"L1 read: "<<adress<<"(tag , index "<<m_currentSetNumber<<endl;
#endif
	int blockColNumber = findTheRequestedBlock();
#ifdef __DEBUG
	cout<<"blockColNumber="<<blockColNumber<<endl;
#endif
	if(blockColNumber == -1)//if it is a miss
	{
#ifdef __DEBUG_OUT
		cout<<"L1 miss "<<endl;
#endif
		//first increment the read miss counter
		m_readMissCounter++;
		blockColNumber = findInvalidBlock(); //in the current set
#ifdef __DEBUG
		cout<<"invalidBlockNumber="<<blockColNumber<<endl;
#endif
		if(blockColNumber == -1) //thre is no invalid blk //corresponds to step1
		{
			//find LRU block the one with max LRU counter
			blockColNumber = findLRUBlockNumber();
			if(blockColNumber == -1)
			{
#ifdef __DEBUG
				cout<<"ERRROR::LRU BLOCK NOT FOUND"<<endl;
#endif
				exit(0);
			}

			//todo::write to next level if it is dirty block
			if(m_victimCacheisEnabled){
				if(swapFromVC(blockColNumber) == true)
				{
					//do nothing
					//m_readMissCounter--; //we incremented it above..so decrement it
				}
				else{
					//read from L2
					if(m_L2Cache!= NULL){
						char appndedAddress[32];
						//cout<<"appendIndexTotheTag l2"<<endl;
						copy(m_currentAdressTag,m_currentAdressTag+32,appndedAddress); //copy the tag
						appendIndexandOffsetTotheTag(appndedAddress); //append the index
						//cout<<"appendIndexTotheTag l2 done"<<endl;
						m_L2Cache->readFromCache(getHexFromBinary(appndedAddress));
					}
					//update state
					m_cache[m_currentSetNumber][blockColNumber].validBit = 1; //valid
					m_cache[m_currentSetNumber][blockColNumber].dirtyBit = 0; //if u missed read its a fresh a block
					copy(m_currentAdressTag,m_currentAdressTag+32,m_cache[m_currentSetNumber][blockColNumber].tag); //tag

				}
				/*findTheRequestedBlockInVC();
				if(found)
					swapLRUBlockofL1withTheRequestedblk(); //copy ststus also
				else {//not found
					finsLRUofVC();
					writeLRUVCtoL2(); //if it is dirty
					copyLRU_L1 to VC_LRU//evict a block from L1
				}*/
				//m_writeBackstoNextCache++; //if it is dirty
			}
			else
			{

				if(m_cache[m_currentSetNumber][blockColNumber].dirtyBit)//if it is dirty
				{
					//writeLRU_L1_toL2();
					if(m_L2Cache!= NULL){

						char appndedAddress[32];
						cout<<"appendIndexTotheTag l2"<<endl;
						copy(m_cache[m_currentSetNumber][blockColNumber].tag,m_cache[m_currentSetNumber][blockColNumber].tag+32,appndedAddress); //copy the tag
						appendIndexandOffsetTotheTag(appndedAddress); //append the index
						cout<<"appendIndexTotheTag l2 done"<<endl;
						m_L2Cache->writeToCache(getHexFromBinary(appndedAddress));
					}
					m_writeBackstoNextCache++;
				}
				//read from L2
				if(m_L2Cache!= NULL){

					char appndedAddress[32];
					cout<<"appendIndexTotheTag l2"<<endl;
					copy(m_currentAdressTag,m_currentAdressTag+32,appndedAddress); //copy the tag
					appendIndexandOffsetTotheTag(appndedAddress); //append the index
					cout<<"appendIndexTotheTag l2 done"<<endl;
					m_L2Cache->readFromCache(getHexFromBinary(appndedAddress));
				}

				//update state
				m_cache[m_currentSetNumber][blockColNumber].validBit = 1; //valid
				m_cache[m_currentSetNumber][blockColNumber].dirtyBit = 0; //if u missed read its a fresh a block
				copy(m_currentAdressTag,m_currentAdressTag+32,m_cache[m_currentSetNumber][blockColNumber].tag); //tag

			}
		}
		else{ //if there is an invalid blk
			if(m_L2Cache!= NULL){

				char appndedAddress[32];
				cout<<"appendIndexTotheTag l2"<<endl;
				copy(m_currentAdressTag,m_currentAdressTag+32,appndedAddress); //copy the tag
				appendIndexandOffsetTotheTag(appndedAddress); //append the index
				cout<<"appendIndexTotheTag l2 done"<<endl;
				m_L2Cache->readFromCache(getHexFromBinary(appndedAddress));
			}

			//no need to check for victim cache
			//update state
			m_cache[m_currentSetNumber][blockColNumber].validBit = 1; //valid
			m_cache[m_currentSetNumber][blockColNumber].dirtyBit = 0; //if u missed read its a fresh a block
			copy(m_currentAdressTag,m_currentAdressTag+32,m_cache[m_currentSetNumber][blockColNumber].tag); //tag


		}
#ifdef __DEBUG
		cout<<"blockColNumber="<<blockColNumber<<endl;
#endif

	}
#ifdef __DEBUG_OUT
	cout<<"L1 victim:: None "<<endl;
	cout<<"L1 update LRU"<<endl;
#endif
	/*its a hit so no need of status update*/
	//update LRU counter others
	updateLRUCounter(m_cache[m_currentSetNumber][blockColNumber].lruCounter);
	//update LRU counter
	m_cache[m_currentSetNumber][blockColNumber].lruCounter = 0; //most recenlty used

#ifdef __DEBUG
	for(int i =0;i<m_cacheAssoc;i++) //traverse through the cols
	{
		cout<<"lru count"<<i<<" "<<m_cache[m_currentSetNumber][i].lruCounter;
	}

	cout<<"readFromCache block found ="<<blockColNumber;
#endif


}
void CcacheSimulator::updateLRUCounter(int currentLruCounter)
{
	for(int i =0;i<m_cacheAssoc;i++) //traverse through the cols
	{
		if(currentLruCounter == -1) //we are replacing an invalid block
		{
			if( m_cache[m_currentSetNumber][i].lruCounter >= 0) //increment all valid blocks
				(m_cache[m_currentSetNumber][i].lruCounter)++; //increment counter
		}
		else //if replacing a valid block
		{
			if(m_cache[m_currentSetNumber][i].lruCounter < currentLruCounter &&  m_cache[m_currentSetNumber][i].lruCounter >= 0)//if less than the current LRU
				(m_cache[m_currentSetNumber][i].lruCounter)++; //increment counter

		}
	}

}
int CcacheSimulator::findLRUBlockNumber()
{
	for(int i =0;i<m_cacheAssoc;i++) //traverse through the cols
	{
		if((m_cache[m_currentSetNumber][i].lruCounter) == m_cacheAssoc -1) //max possible LRU
		{
			return i;
		}
	}
	return -1;
}
int CcacheSimulator::findTheRequestedBlock(){
#ifdef __DEBUG
	cout<<"set number::"<<m_currentSetNumber<<endl;
#endif
	int i;
	for(i =0;i<m_cacheAssoc;i++) //traverse through the cols
	{
		if((m_cache[m_currentSetNumber][i].validBit)==1) //if it is a valid block
		{
			int j;
			//compare the tags
			for(j=0;j<32;j++) //32 is the size;
			{
				//cout<<"col="<<i<<" tagindex="<<j<<"m_currentAdressTag[j]"<<m_currentAdressTag[j]<<"m_cache[m_currentSetNumber][i].tag[j]"<<m_cache[m_currentSetNumber][i].tag[j]<<endl;
				if(m_cache[m_currentSetNumber][i].tag[j] != m_currentAdressTag[j])
				{
					break; //break out of this for loop
				}
			}
			if(j==32) //ddid not break from the prev..thta means a hit
				break; //break out of outer loop

		}

	}
	if(i<m_cacheAssoc) //did not break..that means a
		return i;
	else
		return -1; //not found
}
int CcacheSimulator::findInvalidBlock()
{
	for(int i =0;i<m_cacheAssoc;i++) //traverse through the cols
	{
		if((m_cache[m_currentSetNumber][i].validBit)==0) //invalid block found
		{
			return i; //returns col no
		}

	}
	return -1; //not found
}
void CcacheSimulator::addressDecoder(string addressChar){

	char addressBinary[32] = {'0','0','0','0',	'0','0','0','0', '0','0','0','0', '0','0','0','0',
			'0','0','0','0',  '0','0','0','0', '0','0','0','0', '0','0','0','0',}; //initialize it
	char addressBinaryTag[32] = {'0','0','0','0',	'0','0','0','0', '0','0','0','0', '0','0','0','0',
			'0','0','0','0',  '0','0','0','0', '0','0','0','0', '0','0','0','0',}; //initialize it
	char addressBinaryIndex[32] = {'0','0','0','0',	'0','0','0','0', '0','0','0','0', '0','0','0','0',
			'0','0','0','0',  '0','0','0','0', '0','0','0','0', '0','0','0','0',}; //initialize it
	int index =0; //LSB
	//get the string read it character by character
	int length = addressChar.length();
	for(int i =0;i <= length;i++) { //read the address from reverse direction
		if(addressChar[length-i] =='a'|| addressChar[length-i] =='A')
		{

			addressBinary[index++] = '0';
			addressBinary[index++] = '1';
			addressBinary[index++] = '0';
			addressBinary[index++] = '1';
		}
		else if (addressChar[length-i] =='b'|| addressChar[length-i] =='B')
		{

			addressBinary[index++] = '1';
			addressBinary[index++] = '1';
			addressBinary[index++] = '0';
			addressBinary[index++] = '1';
		}
		else if (addressChar[length-i] =='c'|| addressChar[length-i] =='C')
		{

			addressBinary[index++] = '0';
			addressBinary[index++] = '0';
			addressBinary[index++] = '1';
			addressBinary[index++] = '1';
		}
		else if (addressChar[length-i] =='d'|| addressChar[length-i] =='D')
		{

			addressBinary[index++] = '1';
			addressBinary[index++] = '0';
			addressBinary[index++] = '1';
			addressBinary[index++] = '1';
		}
		else if (addressChar[length-i] =='e'|| addressChar[length-i] =='E')
		{

			addressBinary[index++] = '0';
			addressBinary[index++] = '1';
			addressBinary[index++] = '1';
			addressBinary[index++] = '1';
		}
		else if (addressChar[length-i] =='f'|| addressChar[length-i] =='F')
		{

			addressBinary[index++] = '1';
			addressBinary[index++] = '1';
			addressBinary[index++] = '1';
			addressBinary[index++] = '1';
		}
		else if (addressChar[length-i] =='1')
		{
			addressBinary[index++] = '1';
			addressBinary[index++] = '0';
			addressBinary[index++] = '0';
			addressBinary[index++] = '0';
		}
		else if (addressChar[length-i] =='2')
		{
			addressBinary[index++] = '0';
			addressBinary[index++] = '1';
			addressBinary[index++] = '0';
			addressBinary[index++] = '0';
		}
		else if (addressChar[length-i] =='3')
		{
			addressBinary[index++] = '1';
			addressBinary[index++] = '1';
			addressBinary[index++] = '0';
			addressBinary[index++] = '0';
		}
		else if (addressChar[length-i] =='4')
		{
			addressBinary[index++] = '0';
			addressBinary[index++] = '0';
			addressBinary[index++] = '1';
			addressBinary[index++] = '0';
		}
		else if (addressChar[length-i] =='5')
		{
			addressBinary[index++] = '1';
			addressBinary[index++] = '0';
			addressBinary[index++] = '1';
			addressBinary[index++] = '0';
		}
		else if (addressChar[length-i] =='6')
		{
			addressBinary[index++] = '0';
			addressBinary[index++] = '1';
			addressBinary[index++] = '1';
			addressBinary[index++] = '0';
		}
		else if (addressChar[length-i] =='7')
		{
			addressBinary[index++] = '1';
			addressBinary[index++] = '1';
			addressBinary[index++] = '1';
			addressBinary[index++] = '0';
		}
		else if (addressChar[length-i] =='8')
		{
			addressBinary[index++] = '0';
			addressBinary[index++] = '0';
			addressBinary[index++] = '0';
			addressBinary[index++] = '1';
		}
		else if (addressChar[length-i] =='9')
		{
			addressBinary[index++] = '1';
			addressBinary[index++] = '0';
			addressBinary[index++] = '0';
			addressBinary[index++] = '1';
		}
		else if(addressChar[length-i] =='0')
		{
			addressBinary[index++] = '0';
			addressBinary[index++] = '0';
			addressBinary[index++] = '0';
			addressBinary[index++] = '0';
		}
	}

#ifdef __DEBUGaddressDecoder
	int mod =0;
	cout <<endl;
	for(int p=31;p>=0;p--)
	{

		cout <<addressBinary[p];
		if(mod<3)
		{
			mod++;
		}
		else
		{
			cout <<" ";
			mod =0;
		}
	}
#endif
	//we have the binary address..get the tag from it
	int myIndex=0;
	for(int i = m_blockOffset+m_indexBits;i<32;i++)
	{
		addressBinaryTag[myIndex]= addressBinary[i];
		myIndex++;
	}
#ifdef __DEBUGaddressDecoder0
	int mod =0;
	cout <<endl;
	for(int p=31;p>=0;p--)
	{

		cout <<addressBinaryTag[p];
		if(mod<3)
		{
			mod++;
		}
		else
		{
			cout <<" ";
			mod =0;
		}
	}
#endif
	myIndex=0;
	for(int i = m_blockOffset;i<m_blockOffset+m_indexBits;i++)
	{
		addressBinaryIndex[myIndex]= addressBinary[i];
		myIndex++;
	}

#ifdef __DEBUGaddressDecoder
	mod =0;
	cout <<"index in binary"<<endl;
	for(int p=31;p>=0;p--)
	{

		cout <<addressBinaryIndex[p];
		if(mod<3)
		{
			mod++;
		}
		else
		{
			cout <<" "<<endl;
			mod =0;
		}
	}
#endif
	m_currentSetNumber = binaryToDecimal(addressBinaryIndex); //get the set number
	copy(addressBinaryIndex,addressBinaryIndex+32,m_currentIndexBinary);
#ifdef __DEBUG
	cout<<"current set no"<<m_currentSetNumber<<endl;
#endif
	copy(addressBinaryTag,addressBinaryTag+32,m_currentAdressTag); //thi is the adress after removing offset and index bits
#ifdef __DEBUGaddressDecoder
	mod =0;
	cout <<endl;
	for(int p=31;p>=0;p--)
	{

		cout <<m_currentAdressTag[p];
		if(mod<3)
		{
			mod++;
		}
		else
		{
			cout <<" ";
			mod =0;
		}
	}
#endif
}
int CcacheSimulator::binaryToDecimal(char binary[32]){
	int decimalValue = 0;
	for (int i =0;i<32;i++)
	{
		if(binary[i]=='1')
		{
			decimalValue = decimalValue +pow(2,i);
		}
	}
	return decimalValue;
}
int CcacheSimulator::getWriteMissCounter()
{
	return m_writeMissCounter;
}
int CcacheSimulator::getReadMissCounter()
{
	return m_readMissCounter;
}

int CcacheSimulator::getWriteCounter()
{
	return m_writeCounter;
}
int CcacheSimulator::getReadCounter()
{
	return m_readCounter;
}
int CcacheSimulator::getwriteBackCounter()
{
	return m_writeBackstoNextCache;
}
string CcacheSimulator::getHexFromBinary(char binary[32]){

	int decimal = this->binaryToDecimal(binary);
#ifdef __DEBUG
	cout<<"decimal= "<<decimal<<endl;
#endif
	stringstream ss;
	ss<< std::hex << decimal; // int decimal_value
	return ss.str();
}

void CcacheSimulator::printCacheContents()
{

	for(int i =0;i<m_cacheRows;i++){
		int lruCount;
		int sortedBlocks[m_cacheAssoc];
		std::fill_n(sortedBlocks, m_cacheAssoc, -1);
		for(lruCount=0;lruCount<m_cacheAssoc;lruCount++){
			for(int j=0;j<m_cacheAssoc;j++) //for each LRU count loop through and find out the crspndg blk
			{
				if(m_cache[i][j].lruCounter == lruCount)
					sortedBlocks[lruCount] = j;
			}
		}
		//print the sorted blocks
		cout<<"  set   "<<i<<":   ";
		for(int k=0;k<m_cacheAssoc;k++){
			if(sortedBlocks[k]!=-1){
				string dirtybit = m_cache[i][sortedBlocks[k]].dirtyBit?"D":" ";
				cout<<getHexFromBinary(m_cache[i][sortedBlocks[k]].tag)<<" "<<dirtybit<<" ";
			}
			else
			{
				cout<<"        "<<"   ";
			}
		}
		cout<<endl;
	}

}

void CcacheSimulator::appendIndexandOffsetTotheTag(char* currentTag)
{
	char addressBinary[32] = {'0','0','0','0',	'0','0','0','0', '0','0','0','0', '0','0','0','0',
			'0','0','0','0',  '0','0','0','0', '0','0','0','0', '0','0','0','0',}; //initialize it
	//append offset bits to the address
	/*its already done implicitly ablove*/

	//append index bits to the address
	for(int i=0;i<m_indexBits;i++)
		addressBinary[i+m_blockOffset]=m_currentIndexBinary[i];

	for(int i=0;i<32-(m_blockOffset+m_indexBits);i++)
	{
		addressBinary[i+m_blockOffset+m_indexBits] = currentTag[i];
	}

	copy(addressBinary,addressBinary+32,currentTag);

}
void CcacheSimulator::appendOffsetOnlyTotheTag(char* currentTag)
{
	char addressBinary[32] = {'0','0','0','0',	'0','0','0','0', '0','0','0','0', '0','0','0','0',
			'0','0','0','0',  '0','0','0','0', '0','0','0','0', '0','0','0','0',}; //initialize it
	//append offset bits to the address
	/*its already done implicitly ablove*/

	for(int i=0;i<32-(m_blockOffset);i++)
	{
		addressBinary[i+m_blockOffset] = currentTag[i];
	}

	copy(addressBinary,addressBinary+32,currentTag);

}
void CcacheSimulator::appendIndexOnlyTotheTag(char* currentTag)
{
	char addressBinary[32] = {'0','0','0','0',	'0','0','0','0', '0','0','0','0', '0','0','0','0',
			'0','0','0','0',  '0','0','0','0', '0','0','0','0', '0','0','0','0',}; //initialize it

	//append index bits to the address
	for(int i=0;i<m_indexBits;i++)
		addressBinary[i]=m_currentIndexBinary[i];

	for(int i=0;i<32-(m_indexBits);i++)
	{
		addressBinary[i+m_indexBits] = currentTag[i];
	}

	copy(addressBinary,addressBinary+32,currentTag);

}
bool CcacheSimulator::swapFromVC(int LrublockNumber){
	cout<<"VC::"<<endl;
	int requstedBlkLocation = findTheRequestedBlockinVC() ;
	bool returnValue;
	m_numberofSwapReqs++;
	if(requstedBlkLocation!= -1) // if found
	{
		cout<<"VC::swap"<<endl;
		m_numberofSwaps++;
		memBlock temp;
		temp.validBit = m_VC_cache[requstedBlkLocation].validBit;
		temp.dirtyBit = m_VC_cache[requstedBlkLocation].dirtyBit;
		copy(m_VC_cache[requstedBlkLocation].tag+m_indexBits,m_VC_cache[requstedBlkLocation].tag+32,temp.tag); //no need to copy index bits

		m_VC_cache[requstedBlkLocation].validBit = m_cache[m_currentSetNumber][LrublockNumber].validBit;
		m_VC_cache[requstedBlkLocation].dirtyBit = m_cache[m_currentSetNumber][LrublockNumber].dirtyBit;
		copy(m_cache[m_currentSetNumber][LrublockNumber].tag,m_cache[m_currentSetNumber][LrublockNumber].tag+32,m_VC_cache[requstedBlkLocation].tag);
		//append index to the tag to make it unique
		appendIndexOnlyTotheTag(m_VC_cache[requstedBlkLocation].tag); //send tag and get it appended

		m_cache[m_currentSetNumber][LrublockNumber].validBit = temp.validBit;
		m_cache[m_currentSetNumber][LrublockNumber].dirtyBit = temp.dirtyBit;
		copy(temp.tag,temp.tag+32,m_cache[m_currentSetNumber][LrublockNumber].tag);
		returnValue = true;
		//update Lru
	}
	else
	{
		requstedBlkLocation = findInvalidBlockinVC();
		cout<<"VC1::invalid blk"<<requstedBlkLocation<<endl;
		if(requstedBlkLocation != -1) //found invalid blk
		{
			//copy evrything from the victim block to the invalid block
			m_VC_cache[requstedBlkLocation].validBit = m_cache[m_currentSetNumber][LrublockNumber].validBit;
			m_VC_cache[requstedBlkLocation].dirtyBit = m_cache[m_currentSetNumber][LrublockNumber].dirtyBit;
			copy(m_cache[m_currentSetNumber][LrublockNumber].tag,m_cache[m_currentSetNumber][LrublockNumber].tag+32,m_VC_cache[requstedBlkLocation].tag);
			//append index to the tag to make it unique
			appendIndexOnlyTotheTag(m_VC_cache[requstedBlkLocation].tag); //send tag and get it appended
			cout<<"VC1::lru blk"<<LrublockNumber<<endl;
			cout<<"tag::";
			for(int i=31;i>=0;i--)
						{
							cout<<m_cache[m_currentSetNumber][LrublockNumber].tag[i]<<" ";
						}
			cout<<endl;
			for(int i=31;i>=0;i--)
			{
				cout<<m_VC_cache[requstedBlkLocation].tag[i]<<" ";
			}
		}
		else
		{
			cout<<"VC2::invalid blk"<<requstedBlkLocation<<endl;;
			requstedBlkLocation = findLruinVC();
			if(m_VC_cache[requstedBlkLocation].dirtyBit)//if dirty
			{
				//writeLRU_L1_toL2();
				if(m_L2Cache!= NULL){

					char appndedAddress[32];
					//cout<<"appendIndexandOffsetTotheTag l2"<<endl;
					copy(m_VC_cache[requstedBlkLocation].tag,m_VC_cache[requstedBlkLocation].tag+32,appndedAddress); //copy the original tag(tag+index)
					appendOffsetOnlyTotheTag(appndedAddress); //append the index
					//cout<<"appendIndexandOffsetTotheTag l2 done"<<endl;
					m_L2Cache->writeToCache(getHexFromBinary(appndedAddress));
				}
				m_writeBackstoNextCache++;
			}
			//put victom blk here
			//copy evrything from the victim block to the invalid block
			m_VC_cache[requstedBlkLocation].validBit = m_cache[m_currentSetNumber][LrublockNumber].validBit;
			m_VC_cache[requstedBlkLocation].dirtyBit = m_cache[m_currentSetNumber][LrublockNumber].dirtyBit;
			copy(m_cache[m_currentSetNumber][LrublockNumber].tag,m_cache[m_currentSetNumber][LrublockNumber].tag+32,m_VC_cache[requstedBlkLocation].tag);
			//append index to the tag to make it unique
			appendIndexOnlyTotheTag(m_VC_cache[requstedBlkLocation].tag); //send tag and get it appended
			for(int i=31;i>=0;i--)
						{
							cout<<m_VC_cache[requstedBlkLocation].tag[i]<<" ";
						}
		}
		returnValue = false;
		//update Lru
	}

	updateVCLRUCounter(m_VC_cache[requstedBlkLocation].lruCounter);
	m_VC_cache[requstedBlkLocation].lruCounter = 0; //most recenlty used
	return returnValue;
}

int CcacheSimulator::findTheRequestedBlockinVC(){
	char indexApeendedTag[32];
	copy(m_currentAdressTag,m_currentAdressTag+32,indexApeendedTag);
	appendIndexOnlyTotheTag(indexApeendedTag); //append index to the tag n look
	int i;
	for(i =0;i<m_VC_cols;i++) //traverse through the cols
	{
		if((m_VC_cache[i].validBit)==1) //if it is a valid block
		{
			int j;
			//compare the tags
			for(j=0;j<32;j++) //32 is the size;
			{
				//cout<<"col="<<i<<" tagindex="<<j<<"m_currentAdressTag[j]"<<m_currentAdressTag[j]<<"m_cache[m_currentSetNumber][i].tag[j]"<<m_cache[m_currentSetNumber][i].tag[j]<<endl;
				if(m_VC_cache[i].tag[j] != indexApeendedTag[j])
				{
					break; //break out of this for loop
				}
			}
			if(j==32) //did not break from the prev..thta means a hit
				break; //break out of outer loop

		}

	}
	if(i<m_VC_cols) //did not break..that means a
		return i;
	else
		return -1; //not found
}

int CcacheSimulator::findInvalidBlockinVC()
{
	for(int i =0;i<m_VC_cols;i++) //traverse through the cols
	{
		if((m_VC_cache[i].validBit)==0) //invalid block found
		{
			return i; //returns col no
		}

	}
	return -1; //not found
}

void CcacheSimulator::updateVCLRUCounter(int currentLruCounter)
{
	for(int i =0;i<m_VC_cols;i++) //traverse through the cols
	{
		if(currentLruCounter == -1) //we are replacing an invalid block
		{
			if( m_VC_cache[i].lruCounter >= 0) //increment all valid blocks
				(m_VC_cache[i].lruCounter)++; //increment counter
		}
		else //if replacing a valid block
		{
			if(m_VC_cache[i].lruCounter < currentLruCounter &&  m_VC_cache[i].lruCounter >= 0)//if less than the current LRU
				(m_VC_cache[i].lruCounter)++; //increment counter

		}
	}

}
int CcacheSimulator::findLruinVC()
{
	for(int i =0;i<m_VC_cols;i++) //traverse through the cols
	{
		if((m_VC_cache[i].lruCounter) == m_VC_cols -1) //max possible LRU
		{
			return i;
		}
	}
	return -1;
}

int CcacheSimulator::getMemtraffic()
{
	return (getReadMissCounter()+getWriteMissCounter()+getwriteBackCounter()-m_numberofSwaps);
}

